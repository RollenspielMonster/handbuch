# Fediverse

[Fediverse](https://de.wikipedia.org/wiki/Fediverse) kommt von „Federated Universe“ und ist eine Wortkreuzung aus „Föderation“ und „Universum“. Es handelt sich um ein dezentrales Kommunikationsnetz – wie es sich schon bei der E-Mail bewährt hat. Technisch spielt es kaum eine Rolle, bei welchem Anbieter man ist – die Nutzer\_innen einer Instanz können denen anderer Instanzen folgen. Das ist bei sozialen Netzwerken bisher einzigartig, aber sehr wichtig, denn nur so kann man einen Dienst wechseln, ohne dabei sämtliche Kontakte zu verlieren. Das bedeutet: Nur so haben Nutzende wirklich eine Wahl und nur so wird ein gesamtgesellschaftlicher Diskurs über die Regeln unserer Kommunikation möglich.

## Protokoll

Durch den Einsatz von Sozialer-Netzwerk-Software, die z.B. das Protokoll [ActivityPub](https://de.wikipedia.org/wiki/ActivityPub) unterstützt, können sich unabhängige Server mit dem Fediverse verbinden, so dass seine Benutzer\_innen kurze Nachrichten von anderen Benutzer\_innen auf jedem angeschlossenen Server verfolgen und empfangen können. Das Fediverse basiert auf Freier Software und offenen Schnittstellen.

Wie bei der E-Mail wählt man zuerst einen Anbieter aus. Das sind Server, auf denen freie Software läuft. Diese Server nennt man auch Instanzen oder Nodes. Sie werden in der Regel von Enthusiasten betrieben, und die Benutzung ist meist kostenlos. Viele haben einen thematischen Schwerpunkt.

Genauer gehen wir darauf in dem Kapitel [ActivityPub](/fediverse/activitypub.html) ein.

# Die Vernetzung

![Plattforme/Protokolle im Fediverse](img/fediverse.png)

Auf einem Schaubild ist immer nur begrenzt Platz – das Fediverse ist viel größer, was die nach außen gehenden blauen Linien darstellen sollen. Irgendwann geht dann aber die Übersichtlichkeit verloren und letztendlich haben es dann nur die bekanntesten/populärsten Vertreter des Fediverse auf die Grafik geschafft.

Die Vernetzung kann man sich hier Visualisieren lassen: https://www.fediverse.space

# Warum Alternativen

Es sollte mittlerweile jedem klar geworden sein das man bei dem Geschäftsmodell von Facebook, Twitter und Co. nicht der Kunde, sondern das Produkt ist. Dies funktionieren aber nur, wenn Nutzer\_innen seine Daten bereitwillig zur Verfügung stellen. Im Austausch gegen die Daten erhalten Nutzer\_innen dann die Möglichkeit, die Dienste der Konzerne "kostenlos" zu nutzen; wobei man eigentlich dazu "unentgeltliche" sagen müsste.

Um die Daten der Nutzer\_innen zu bekommen wird diesen eine Illusion von Kontrolle vorgetäuscht und dabei wird ganz bewusst auf [Dark Pattern](https://de.wikipedia.org/wiki/Dark_Pattern) bzw. [Nudging](https://de.wikipedia.org/wiki/Nudge) zurückgegriffen. z.B. Facebook beherrscht es meisterhaft Datenschutz-Einstellungen zu verstecken, missverständlich darzustellen oder mit irreführenden Formulierungen den Nutzer\_innen von derartigen Einstellungen abzuhalten.

**Ohne dieses Vorgehen wäre das Geschäftsmodell nicht wirtschaftlich tragbar.**

## Welche Alternativen

### Vergleichbar mit Facebook

- [Friendica](https://friendi.ca/)
- [Hubzilla](https://hubzilla.org//page/hubzilla/hubzilla-project)
- [Dispora](https://diasporafoundation.org/)

### Vergleichbar mit Facebook Events

- [Mobilizon](https://joinmobilizon.org/de/)

### Vergleichbar mit Twitter

- [Mastodon](https://joinmastodon.org/)
- [Pleroma](https://pleroma.social/)
- [GNU-Social](https://gnu.io/social/)

### Vergleichbar mit Instagram

- [Pixelfed](https://pixelfed.org/)

### Vergleichbar mit Youtube

- [PeerTube](https://joinpeertube.org/)

### Vergleichbar mit Soundcloud

- [Funkwhale](https://funkwhale.audio/)

### Vergleichbar mit Reddit

- [Lemmy](https://lemmy.ml/)

## Verschiedene Dienste Verbinden

Oft habe ich die Frage gelesen wie man Dienst A mit Dienst B verbindet und die Antwort ist ganz einfach: **Garnicht!**
Der Sinn des Fediverse ist, dass alle der oben stehenden Dienste alleine für sich stehen und miteinander kommunizieren können. Sucht euch einfach das raus, was euch am besten gefällt und meldet euch dort an, natürlich könnt ihr mehrere Dienste auf einmal verwenden.
