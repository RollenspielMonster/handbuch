# Jitsi

## Browser

Wir haben bisher mit einem Chromium basierten Browser die besten erfahrungen bei Meetings gemacht.

![Übersicht](img/overview.jpg)

Die Funktionen befinden sich am unteren Fensterrand und sind aufgeteilt in Mitte, Links und Rechts.

## MITTE

In der Mitte befinden sich von links nach rechts folgende drei Schaltflächen: 4. Mikrofon, 5. Auflegen, 6. Kamera

4. Mikrofon: Hier können Sie Ihr Mikrofon mit einem Klick Ein- oder Ausschalten. Falls weitere Mikrofone angeschlossen sind, können Sie diese in den erweiterten Einstellungen ansteuern.

5. Auflegen: Hiermit beenden Sie die Teilnahme oder das Meeting. Sie können auch Teilnehmer\_innen – leider nur jeden Teilnehmer einzeln - aus dem Meeting „hinauswerfen“.

6. Kamera: Hier können Sie Ihre Kamera mit einem Klick „Ein- oder Ausschalten“. Falls weitere Kameras angeschlossen sind, können Sie diese in den erweiterten Einstellungen ansteuern.

## LINKS:

1. Bildschirm freigeben: Hier können Sie Ihren ganzen Bildschirm oder ein einzelnes Fenster freigeben, so dass alle anderen Teilnehmer\_innen diesen/s auf dem eigenen Bildschirm sehen können. Bei Fenster werden dabei alle geöffneten Fenster angezeigt. Sie wählen das entsprechende Fenster aus und drücken auf „Teilen“. So können vorbereitete Grafiken, Bilder oder Präsentationen anderen Teilnehmer\_innen gezeigt werden.
Ihre eigene Kamera wird automatisch deaktiviert, wenn Sie diese Funktion aktivieren.

2. Hand heben: Beim Betätigen dieser Taste, erscheint im Fenster des Teilnehmers (siehe im Bild Übersicht „Jitsi Meet“ Punkt 11) oben links ein kleines Handsymbol, als Zeichen, dass man eine Frage hat oder einen Beitrag wünscht.
Die Hand verschwindet unter Umständen nach kurzer Zeit wieder und man muss evtl. die Taste abermals betätigen.
Als Organisator ist in diesem Zusammenhang die Funktion „Kacheln“ unter Punkt 7. wichtig. Dabei werden alle Teilnehmer\_innen gleichmäßig auf dem gesamten Monitor verteilt. So haben Sie eine bessere Übersicht, wenn Teilnehmer\_innen Fragen stellen oder Antworten geben (möchten). Je mehr Teilnehmer\_innen desto vorteilhafter ist ein groß dimensionierter Bildschirm des Organisators.

3. Einen Chat starten: Hier kann neben der Videokonferenz ein Chat gestartet werden. Ggfs. müssen die Teilnehmer\_innen diesen ebenfalls aufrufen.
Diese Funktion ist sehr hilfreich, weil Teilnehmer\_innen hier auch Fragen einstellen können, welche dann zu einem gegeben Zeitpunkt beantwortet werden können.
Auch die Teilnehmenden können so schon Fragen oder Meinungen vorab in den Chat schreiben.

## RECHTS

7. Kacheln: Hier können Sie wechseln zwischen: alle Teilnehmer\_innen anzeigen oder Redner im Vordergrund anzeigen.
Ist Kacheln aktiviert, werden alle Teilnehmer\_innen gleichmäßig auf dem Bildschirm verteilt. Der jeweilige Redner wird mit einem blauen Rahmen umrahmt. Ein Stern symbolisiert, dass dieser Benutzer der Organisator des Meetings ist.
In dieser Ansicht werden aber leider geteilte Folien nicht richtig erkannt, da diese meist zu klein dargestellt werden.
Bei ausgeschalteter Kachelfunktion ist automatisch immer der Redner im Vordergrund. Die Teilnehmer\_innen sind im rechten Feld und werden je nach Anzahl und Redeaktivität ein- und ausgeblendet.

8. „+“ Diese Funktion scheint nicht zu funktionieren. Hier können Sie theoretisch über die Eingabe einer Telefonnummer Teilnehmer\_innen einladen.

9. Teilnehmer\_innen über einen Link einladen und PASSWORT vergeben Hier sehen Sie den Link für Ihr Meeting. Wenn Sie auf Kopieren drücken wird der Link und ein kleiner Einladungstext mit Ihrem Namen (wenn unter Einstellungen eingegeben) automatisch in die Zwischenablage kopiert. Zudem müssen(!) Sie für dieses Meeting auch ein Passwort erstellen.
