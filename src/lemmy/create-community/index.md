
# Community erstellen

## Bestehende Communitys

Bevor ihr eine neue Community eröffnet, schaut bitte, ob es bereits eine bestehende Community gibt, siehe dazu [Eine Community finden](/lemmy/find-community/index.html#suchen).

Ansonsten, hier ein paar Punkte, die ihr beachten solltet:

- erstellt eine aussagekräftige und für alle verständliche Beschreibung, siehe [Seitenleiste](/lemmy/create-community/index.html#seitenleiste)
- Bei der Verwendung von Community-Icons und Banner-Grafiken sind die Lizenzbestimmungen zu beachten
- Moderiert eure Community aktiv gemäß unserer [Charta](https://rollenspiel.monster/charta/) bzw. den Bestimmungen der jeweiligen Instanz

## Die Felder

### Name

**Kann nicht mehr geändert werden.**
Wird zur URL der Community. Nur Buchstaben, Zahlen und Unterstriche sind erlaubt. Maximal 16 Zeichen.

### Anzeigename

Wird als Titel der Community angezeigt.

### Icon

Kleines Bild, das in der Community liste vor dem Namen angezeigt wird

### Banner

Größeres Bild, das in der Community Übersicht oben angezeigt wird.

### Seitenleiste

Platz für eine Beschreibung oder Regelungen für die Community.

### NSFW

Damit könnt ihr Beiträge ausblenden, der andere verstören könnte. Z.B. gewalttätige oder sexuelle Szenen. Dies könnt ihr in den Benutzereinstellungen aktivieren oder deaktivieren.
