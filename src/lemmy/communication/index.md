# Kommunikation mit anderen Diensten

Hier wird aufgeschlüsselt, welche Funktionen von anderen Diensten bei Lemmy ankomme und wie diese interpretiert werden.

## Mastodon

Es ist möglich:

- Beiträge in Communitys zu erstellen, dabei wird der erste Absatz als Überschrift verwendet
- auf bestehende Beiträge zu Antworten
- Bilder in Posts von Mastodon in Lemmy darzustellen
- Beiträge zu favorisieren, dies wird in Lemmy als "Hochstimmen(Upvote)" eingetragen

Eingeschränkt möglich:

- Bilder in Posts von Lemmy in Mastodon darzustellen aber nur als Linkvorschau

Aktuell ist es nicht möglich:

- Communitys zu erstellen
- Bilder in Kommentaren darzustellen
- Texte zu formatieren

## Friendica

Es ist möglich:

- auf bestehende Beiträge zu Antworten
- Beiträge mit 👍 Mag ich oder 👎 Mag ich nicht zu bewerten, dies wird in Lemmy als "Hochstimmen(Upvote)" bzw. "Runterstimmen(Downvote)" eingetragen
- Bilder in Posts von Lemmy in Friendica darzustellen und umgekehrt
- Bilder in Kommentaren von Lemmy in Friendica darzustellen
- Beiträge in Communitys zu erstellen, bei der Erstellung ohne Überschrift wird der erste Absatz als Überschrift verwendet

Aktuell ist es nicht möglich:

- Communitys zu erstellen
- Bilder in Kommentaren von Friendica in Lemmy darzustellen

## Hubzilla

Unbekannt, gerne dürfen weitere Infos per [Issue](https://codeberg.org/RollenspielMonster/handbuch/issues) oder [PR](https://codeberg.org/RollenspielMonster/handbuch/pulls) mitgeteilt werden.

## Pixelfed

Aktuell ist es nicht möglich:

- zwischen den Diensten zu kommunizieren

## PeerTube

Es ist möglich:

- PeerTube Kanälen zu folgen

Aktuell ist es nicht möglich:

- von PeerTube aus mit Lemmy zu kommunizieren