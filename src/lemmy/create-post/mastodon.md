# Von Mastodon aus

Stand:
- Mastodon v4.2.1
- Lemmy v0.18.5

## Beitrag

Um einen Beitrag von Mastodon aus zu erstellen, der als Lemmy-Beitrag angezeigt wird, müssen Sie die folgenden Schritte beachten:

1. **Einloggen in Mastodon**:
   Logg dich in deinem Mastodon-Konto ein.

2. **Öffentlicher Beitrag ohne CW**:
   Erstellen einen **öffentlichen** Beitrag in Mastodon, **ohne** einen Content Warning (CW) zu verwenden.

3. **Titel für den Lemmy-Beitrag**:
   Schreib den Titel für deinen Lemmy-Beitrag in die erste Zeile deines Mastodon-Beitrags. Zum Beispiel: "Neue Gaming-Tipps für November!"

4. **Inhalt des Mastodon-Beitrags**:
   Füge den gesamten Inhalt deines Mastodon-Beitrags ein, den du teilen möchtest. Denk dran, alles, was du in Mastodon schreibst, wird auch im Lemmy-Beitrag angezeigt, einschließlich des Titels. Erwähnungen von Usern sowie Hashtags werden auf die Instanz von der du Postest verlinkt.

5. **Erwähnung der Lemmy-Community**:
   Am Ende deines Mastodon-Beitrags erwähne die Lemmy-Community, zu der du deinen Beitrag hinzufügen möchtest. Du kannst dies tun, indem du den Community-Namen mit einem "@"-Symbol voranstellst, z.B., `@systeme@rollenspiel.forum`. Das fügt deinen Mastodon-Beitrag zur [Systeme](https://rollenspiel.forum/c/systeme)-Community hinzu.
   **INFO:** Um einfach an den Namen zu kommen können sie den Link zu der Community in die Mastodon Suche eingeben (1), dann den Account anklicken und im Kontextmenü (2) die Erwähnen Funktion (3) nutzen.
   ![Suche](img/search.png)

6. **Veröffentlichen des Mastodon-Beitrags**:
   Sobald Sie den Beitrag geschrieben haben und sicherstellen, dass er den Anforderungen entspricht (öffentlich, ohne CW, Titel in der ersten Zeile, Erwähnung der Lemmy-Community am Ende), veröffentlichen Sie ihn in Mastodon.

Ihr Mastodon-Beitrag wird nun in Lemmy als Beitrag angezeigt, wobei der Text in der ersten Zeile des Mastodon-Beitrags als Titel für den Lemmy-Beitrag verwendet wird. Alle anderen Informationen aus Ihrem Mastodon-Beitrag werden ebenfalls im Lemmy-Beitrag angezeigt.

Bitte beachten Sie, dass dies von den individuellen Einstellungen und Richtlinien Ihrer Mastodon-Instanz und der Lemmy-Community abhängen kann. Es ist ratsam, sich über die spezifischen Regeln und Vorgehensweisen Ihrer jeweiligen Plattformen zu informieren.

## Kommentar

Um auf einen Beitrag in Lemmy zu Antworten, kompieren sie den Link des Beitrags in das suchfeld wie im vorigen Schritt 5 beschrieben. Danach muss nur noch auf Antworten geklickt werden und ihr könnt einen Kommentar in lemmy über mastodon erstellen.