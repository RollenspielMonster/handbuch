# Web-Oberfläche, Apps und Clients

## Die offizielle Web-Oberfläche

![interface.png](img/interface.png)

Diese Webanwendung, oder das Web User Interface (UI), sind sowohl mit Desktop-Browsern am PC als auch mit gängigen Browsern auf dem Smartphone nutzbar.

Sie können den Inhalt des Feeds mit Hilfe der Filter ändern:

**Abonniert** enthält die Beiträge von Personen auf Ihrem Server und auf anderen Servern, wichtig ist nur, dass es eine Gemeinschaft ist, der Sie folgen.

**Lokal** ist der Feed mit allen Beiträgen von Gemeinschaften auf deinem Server. Auf vielen Servern, insbesondere auf kleineren und auf ein bestimmtes Thema ausgerichteten Servern, ist dies der Ort, an dem die Magie stattfindet. Du kannst von hier aus Leuten antworten, und es ist ein großartiger Ort, um Leute zu treffen, die dieselben Interessen haben wie du.

**Alle**, oder die **Federated Timeline**, ist eine Ansicht aller öffentlichen Beiträge, die Ihren Servern aus dem gesamten Netzwerk (einschließlich lokaler Beiträge) bekannt sind. Der häufigste Grund, warum etwas in der föderierten Zeitleiste erscheint, ist, dass jemand von Ihrem Server einer Gemeinschaft auf einem anderen Server folgt.

| Typ             | Beschreibung                                                          |
| --------------- | --------------------------------------------------------------------- |
| Posts           | Zeigt nur Veröffentlichungen an                                       |
| Kommentare      | Zeigt nur Kommentare an                                               |
| \-\-\-          | \-\-\-                                                                |
| Active          | Sortiert nach der Punktzahl und dem Zeitpunkt des letzten Kommentars. |
| Hot             | Basierend auf der Punktzahl und der Erstellungszeit des Beitrags.     |
| Neu             | Die neuesten Artikel.                                                 |
| Most Comments   | Die Beiträge mit den meisten Kommentaren.                             |
| Neue Kommentare | Die Beiträge mit den neuesten Kommentaren, Sortierung wie im Forum.   |
| Top             | Die Beiträge mit der höchsten Punktzahl im angegebenen Zeitrahmen.    |

[Details zur Beitrags- und Kommentarrangliste] (https://join-lemmy.org/docs/en/about/ranking.html).

## Mobile Apps

### Android

- [**Lemmur**](https://github.com/LemmurOrg/lemmur) - [Google Play Store](https://play.google.com/store/apps/details?id=com.krawieck.lemmur) - [F-Droid](https://f-droid.org/en/packages/com.krawieck.lemmur/)
- [**Jerboa**](https://github.com/dessalines/jerboa) - [Google Play Store](https://play.google.com/store/apps/details?id=com.jerboa) - [F-Droid](https://f-droid.org/en/packages/com.jerboa)

### iOS

- [**Remmel**](https://github.com/uuttff8/Lemmy-iOS) - [App Store](https://apps.apple.com/us/app/remmel-for-lemmy/id1547988171)
