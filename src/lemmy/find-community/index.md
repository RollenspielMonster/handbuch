
# Community finden und abonnieren

## Suchen

Bisher gibt es leider nur externe Tools welche über mehrere Instanzen hinweg Communitys suchen können. Lemmy selber unterstützt diese Funktion noch nicht.

Hier einige Beispiele:
- der [Lemmy Community Browser](https://browse.feddit.de) von [Feddit](https://feddit.de)
- der [Lemmy Explorer](https://lemmyverse.net/communities)

## Verbinden

### Beispiellinks

```
Unsere Instanz: https://lemmy.rollenspiel.monster
Community der Remote Instanz: https://feddit.de/c/rollenspiele
```

### Suchen der Community

Um eine Community auf einer Remote Instanz zu finden, ist der einfachste Weg, den Link zur Community in die Suche einzugeben.

1) als Erstes öffnet man die Suche
![Suche öffnen](img/open-search.png)

2) dann trägt man den Link in das Suchfeld ein und drückt auf Suchen und kann darunter die Community auswählen
![Suche](img/search.png)

3) Rechts erscheint eine Box allen wichtigen Optionen: Abonnieren, Erstellen eines Beitrags und das Blockieren der Community
![Community Menü](img/com-menu.png)

### Community manuell aufrufen

Es gibt auch noch die Möglichkeit die Community manuell aufzurufen, dazu müsst ihr wie folgt den Link selbst zusammenbauen:

`https://lemmy.rollenspiel.monster/c/rollenspiele@feddit.de`

