# Flohmarkt (AP)

Der Flohmarkt im Fediverse ist eine einzigartige Plattform, die es Benutzern ermöglicht, Waren anzubieten, zu verkaufen und zu tauschen. Ähnlich wie bei einem traditionellen Flohmarkt, bietet diese digitale Version eine Möglichkeit, gebrauchte oder selbstgemachte Artikel zu präsentieren und potenzielle Käufer zu finden. Es ist wichtig zu beachten, dass der Flohmarkt im Fediverse selbst keine Zahlungen abwickelt, sondern vielmehr als Vermittler für den Austausch von Waren fungiert, ähnlich wie es bei Offline-Flohmärkten der Fall ist.
