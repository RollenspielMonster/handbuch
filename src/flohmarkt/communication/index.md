# Kommunikation im Fediverse

**Innerhalb von Flohmarkt:**
1. Navigiere zur Flohmarkt-Seite und logge dich in deinen Account ein.
2. Suche entweder direkt auf der Flohmarkt-Plattform nach dem Artikel oder verwende die URL, wenn der Artikel auf einer anderen Instanz ist.
3. Klicke auf den Artikel, um zu den detaillierten Informationen zu gelangen.
4. Rechts neben den Bildern und dem Beschreibungstext findest du eine Chatbox.
5. Verfasse eine Nachricht in der Chatbox, um mit dem Verkäufer in Kontakt zu treten.

**Außerhalb von Flohmarkt:**
1. Kopiere den Link zum Artikel, den du interessant findest.
2. Wechsle zur Instanz, auf der du registriert bist (z. B. Mastodon).
3. Füge den Artikel-Link in die Suchleiste deiner Instanz ein.
4. Suche nach dem Artikel und klicke darauf, um die Details anzuzeigen.
5. Antworte auf den Artikel mit einer privaten Nachricht. In Mastodon heißt die Funktion "Nur erwähnte Profile".
6. Kläre hier alle weiteren Details direkt mit dem Verkäufer.
