# Artikel erstellen

Schritt-für-Schritt-Anleitung zum Erstellen eines Artikels auf dem Flohmarkt:

1. **Einloggen:**
   - Logge dich mit deinen erstellten Anmeldedaten auf der Flohmarkt-Seite ein.

2. **Navigieren zur Artikel-Erstellungsseite:**
   - Nach erfolgreichem Einloggen findest du in der rechten oberen Ecke der Seite ein kleines Pluszeichen (+).
   - Klicke auf dieses Pluszeichen, um zur Eingabemaske für die Artikel zu gelangen.

3. **Eingabemaske ausfüllen:**
   - In der Artikel-Eingabemaske gibt es drei Hauptfelder:
     - **Bilder**
     - **Titel:** Gib einen aussagekräftigen Titel für deinen Artikel ein, um das Interesse potenzieller Käufer zu wecken.
     - **Preis:** Setze einen Preis für deinen Artikel fest. Dieser sollte klar und verständlich angegeben werden.
     - **Beschreibung:** Verfasse eine detaillierte Beschreibung deines Artikels. Hier kannst du den Zustand, Besonderheiten und weitere relevante Informationen teilen.

    **Wichtig:** Aktuell ist es nur möglich die Beschreibung im nachinein zu bearbeiten, also überlege dir gut wie du die Felder ausfüllst. Ansonsten musst du den Artikel neu erstellen.

4. **Artikel veröffentlichen:**
   - Nachdem du alle erforderlichen Informationen eingegeben und Bilder hochgeladen hast, klicke auf die Schaltfläche zum Veröffentlichen oder Ähnlichem (der genaue Begriff kann je nach Plattform variieren).
