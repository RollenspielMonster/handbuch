# Unterschiede zu Twitter

**Mastodon wird gerne mit Twitter verglichen, aber es gibt einige kleinere und größere Unterschiede. Hier versuchen wir, alle Nennenswerten davon aufzulisten.**

- Mastodon ist dezentral. Jede Mastodon-Instanz ist unabhängig von anderen, tauscht aber normalerweise Nachrichten mit anderen aus. Jede Instanz hat ihre eigenen Nutzungsbedingungen und Gepflogenheiten. Bei Twitter gibt es dagegen nur eine zentrale Instanz und nur eine Betreiberfirma.

- Timelines (zu deutsch [Zeitleisten](/mastodon/timelines/)), also Listen von [Toot](/mastodon/toots/#beitr%C3%A4ge-toots-tr%C3%B6ts)'s, sind in Mastodon streng chronologisch geordnet. Bei Twitter wird die Timeline standardmäßig nicht chronologisch sortiert, sondern durch Algorithmen nach Relevanz für den/die jeweilige\_n Nutzer\_in gewichtet und sortiert. So etwas gibt es bei Mastodon nicht.

- Beiträge auf Mastodon können bis zu 500 Zeichen lang sein. Bei Twitter sind es 280 Zeichen.

- Bei Twitter können Beiträge durch "Re-Tweeten" weiterverbreitet werden. Dabei lässt sich optional ein eigener Kommentar voranstellen. Bei Mastodon können Beiträge "geboostet" werden. Das entspricht dem Re-Tweet ohne Kommentar. Eine Möglichkeit zum Boosten mit Kommentar gibt es bei Mastodon nicht, [siehe dazu](/mastodon/toots/replies-threads.html?highlight=kommentar#beitrag-zitieren).

- Bei Twitter gibt es Werbung in Form von gesponserten Tweets, denn damit finanziert der Betreiber den Dienst. Bei Mastodon gibt es keine Werbung, die Finanzierung erfolgt auf anderen Wegen.

- Während bei Twitter die Aktivitäten von Nutzer\_innen sowohl durch Twitter als auch durch Google Analytics und andere Dienste getrackt werden, gibt es bei Mastodon kein Tracking. Es gibt auch keine erzwungenen Redirects wie bei Twitter, wo jeder Aufruf einer externen Domain über `t.co` umgeleitet und erfasst wird.

- Mastodon erlaubt feinere Bestimmungen zur [Sichtbarkeit von Beiträgen](/mastodon/toots/visibility.html), als das bei Twitter der Fall ist.

- Während die Suche nach Tweets und Personen bei Twitter umfassend ist, erfasst diese bei Mastodon immer nur die Toots und Profile, die einer Instanz "bekannt" sind. Das sind also Beiträge von Benutzer\_innen, denen die Nutzer\_innen der jeweiligen Instanz folgen. Eine allumfassende Suche ist aufgrund der Dezentralität des Mastodon Netzwerks sehr aufwändig, und nicht im Funktionsumfang von Mastodon selbst enthalten.

- Anders als Twitter fallen aktuell Mastodon-Instanzen alleine schon aufgrund der Nutzer\_innenzahlen nicht unter das [Netzwerkdurchsetzungsgesetz](https://de.wikipedia.org/wiki/Netzwerkdurchsetzungsgesetz).

## Umzug zu Mastodon

### Kontakte

Häufig scheitert der Wechsel an der Bequemlichkeit. Man möchte die über Jahre gesammelt Kontakte nicht verlieren bzw. die alle bei Mastodon suchen. Falls sie denn überhaupt dort sind.

Aber man muss sie aber gar nicht einzeln suchen. Werkzeugen wie [Fedifinder](https://fedifinder.glitch.me/) oder [Debirdify](https://pruvisto.org/debirdify) durchsuchen die Twitter-Kontakte nach Erwähnungen von Mastodon-Accounts. Sind die Kontakte schon bei Mastodon angemeldet, kann man sie automatisch der dortigen Freundesliste hinzufügen lassen. Solche Tools sind natürlich darauf angewiesen, dass Menschen ihre Mastodon-Accounts auf Twitter hinterlegen – das sollte man also auch selbst tun, falls nicht schon geschehen.

Debirdify gibt außerdem eine Orientierungshilfe bei der Auswahl der Mastodon-Instanz. Wenn man seine Twitter-Kontakte durchsuchen lässt, erstellt Debirdify eine Übersicht der Instanzen, auf denen die eigenen Kontakte angemeldet sind.

### Reichweite

Einige wollen auch ihre Reichweite nicht verlieren, die sie sich in all der Zeit bei Twitter aufgebaut haben, dies kann aber mit [Crosspostern](/mastodon/toots/crossposting.md) abgefangen werden.
