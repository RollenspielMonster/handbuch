# Einstellungen

## Aussehen

### Interface-Sprache

Wählen Sie, in welcher Sprache Mastodon mit Ihnen kommuniziert.
Dies hat keine Auswirkungen auf die Beiträge in Ihren Zeitleisten, da Mastodon keine Übersetzungsfunktion hat.

### Theme

Wählen Sie zwischen dem Standardthema, einem kontrastreichen Thema und einem hellen Thema. Beim kontrastreichen Thema werden Links und [Hashtag](/mastodon/toots/hashtags.html)'s in Blau angezeigt.

### Fortgeschrittene Benutzeroberfläche

Hier können Sie zwischen der einfachen Schnittstelle und der erweiterten Schnittstelle wählen.

### Animationen und Barrierefreiheit

Hier kannst du wählen, ob die Timelines automatisch neue Toots laden oder nicht, ob sich Gifs automatisch bewegen oder nicht, ob Animationen angezeigt werden, z.B. für das Boosten oder Liken eines Beitrags, ob du Swiping-Bewegungen verwenden kannst, um mit Mastodon zu interagieren und ob Mastodon seine eigenen Schriftarten anzeigt oder auf die Standardschriftarten deines Systems zurückgreift.

### Beitragslayout

#### Bilder in nicht ausgeklappten Beiträgen auf 16:9 zuschneiden

Wir empfehlen, dieses Kästchen zu deaktivieren, da die Beiträge besser aussehen, wenn das ganze Bild angezeigt wird.

### Entdecken

#### Heutige Trends anzeigen

Hier können Sie die aktuellen Hashtags in der einfachen oder erweiterten Benutzeroberfläche ausblenden.

### Bestätigungsfenster

Dies sind einige gute Optionen, um zu vermeiden, dass ein Beitrag versehentlich geboostet oder gelöscht wird oder jemandem nicht mehr gefolgt wird.

### NSFW

Mit den Einstellungen für sensible Inhalte können Sie alle Medien oder alle vom Poster als sensibel gekennzeichneten Medien ein- oder ausblenden.
Wenn Sie sich nicht so leicht aus der Ruhe bringen lassen, können Sie auch alle Beiträge mit Inhaltswarnungen einblenden, damit Sie nicht jedes Mal auf die Inhaltswarnung klicken müssen, um den gesamten Inhalt zu sehen.

## Benachrichtigungen

Die meisten Benachrichtigungseinstellungen wurden bereits [erwähnt](/mastodon/mitteilungen/index.html), daher finden Sie hier nur Optionen für E-Mail-Benachrichtigungen sowie Optionen zum Blockieren aller Benachrichtigungen und/oder Direktnachrichten von Personen, denen Sie nicht folgen, oder von Personen, die Ihnen nicht folgen.

## Weiteres

### Indizierung durch Suchmaschinen deaktivieren

Damit wird Ihr Profil aus den Suchergebnissen von Duck Duck Go, Google und anderen Suchmaschinen ausgeblendet.

### Gruppiere erneut geteilte Beiträge auf der Startseite

Diese Funktion ist standardmäßig aktiviert. Wenn Sie diese Funktion deaktivieren, kann dies dazu führen, dass derselbe Beitrag in Ihrer Timeline mehrmals hintereinander angezeigt wird, wenn er von vielen Personen geboostet wurde.

### Standardeinstellungen für Beiträge

Hier können Sie die Standard-Privatsphären-Einstellung für Ihre Beiträge ändern, in welcher Sprache Ihre Beiträge verfasst sind (wenn Sie nur in einer Sprache posten), ob alle Ihre Medien standardmäßig als sensibel gekennzeichnet sind (z. B. wenn Sie viele Nacktbilder posten) und ob Sie die von Ihnen verwendete Anwendung in Ihren Beiträgen offenlegen möchten. Die letzte Option hat keine Auswirkung, wenn Sie Mastodon nur im Browser verwenden.

### Sprachen filtern

Hier kannst du Beiträge in Sprachen herausfiltern, die du nicht verstehst. Wählen Sie die Sprachen aus, die Sie verstehen, und lassen Sie die anderen Kästchen unangetastet.
