# Authorisierte Anwendungen

Hier sehen Sie, welche Apps mit Ihrem Konto verbunden sind. Hier werden z. B. [mobile Apps](/mastodon/apps-clients.html#mobile-apps) angezeigt, die Sie auf Ihrem Telefon verwenden.
Wenn Sie auf "widerrufen" klicken, werden Sie von dieser App abgemeldet.
