# Filter

Mit Filtern können Sie Beiträge auf der Grundlage von Wörtern oder Begriffen ausblenden, die in den Beiträgen enthalten sind.
Damit können Sie z. B. Retweets (RT @), Wörter, die Sie stören, oder Diskussionen, die Sie einfach nicht interessieren, herausfiltern.
