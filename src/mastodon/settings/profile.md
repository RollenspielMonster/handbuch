# Profil

## Aussehen

### Anzeigename

Hier können Sie den Namen wählen, der auf Ihrem Profil und in Ihren Beiträgen angezeigt wird.
Ein Anzeigename kann Leerzeichen und Emoji enthalten.

### Über mich

Hier können Sie einen kurzen Text über sich schreiben, um den Besuchern Ihres Profils mitzuteilen, wer Sie sind.

### Titelbild

Das Kopfzeilenbild wird nur angezeigt, wenn Besucher Ihr Profil besuchen. Es wird oben wie ein Banner angezeigt.

### Profilbild

Der Avatar ist Ihr Profilbild, das nicht nur auf Ihrem Profil, sondern auch auf allen Ihren Toots angezeigt wird.
Ein Avatar kann transparent sein.

### Profil sperren

besser wäre: Folgeanfragen verlangen

Wählen Sie, ob Sie möchten, dass jeder Ihnen sofort folgen kann, oder ob Sie Folgeanfragen genehmigen möchten.

### Dieses Profil ist ein Bot

Diese Option bewirkt nicht viel, außer Ihr Profil als Bot zu kennzeichnen. Diese Option wird z. B. gewählt, wenn Sie überwiegend Crossposts von Twitter aus machen oder Ihre Beiträge über RSS füttern.

### Dieses Profil im Profilverzeichnis zeigen

Mit dieser Option wird Ihr Profil im Profilverzeichnis und in den Follow-Vorschlägen angezeigt.

### Netzwerk ausblenden

Blendet aus wem du folgst und wer dir folgt

### Tabellenfelder

Hier können Sie 4 Benutzerfelder definieren. Viele Leute nutzen dies, um auf ihre Homepage(s) zu verlinken oder um ihre Pronomen anzuzeigen.

### Verifizierung

Mastodon kann die Links, die im Profil eingetragen werden, mit Querverweisen versehen, um zu beweisen, dass du der tatsächliche Eigentümer dieses Links bist. Wenn es sich bei diesem Links um deine persönliche Homepage handelt oder der Projekthomepage, kann sie als die nächstbeste Möglichkeit der Identitätsüberprüfung dienen.

Da Mastodon dezentral ist, gibt es keinen besseren Weg deine Identität zu bestätigen, als einen Link zu deiner eigenen Webseite, der die Leute bereits vertrauen.

1. Fügt den Link zu eurer Webseite in eins der Tabellenfelder ein.
2. Rechts daneben seht ihr eine Box mit der Überschrift "Verifizierung" und darunter einen HTML String.
3. Kopiert diesen String und fügt ihn in eure Homepage in den [body tag](https://www.w3schools.com/html/html_basic.asp), aber so das er auf der Hauptseite geladen wird.

Tipp: Ihr könnt den kopierten Link auch mit `style="display: none;"` ausblenden lassen, ohne dass die Verifikation beeinträchtigt wird. Falls ihr den Link lieber auf einer Kontaktseite sichtbar platziert haben wollt.

Wichtig: Die Domain muss ein gültiges SSL Zertifikat haben und darf nicht hinter weiterleitungen verborgen sein.

Beispiel:

```html
 <!DOCTYPE html>
<html>
<body>

<a rel="me" style="display: none;" href="https://rollenspiel.social/@EUERNAME"></a>

<h1>Meine Webseite</h1>
<p>Content</p>

</body>
</html> 
```

### Konto umziehen, löschen

Dazu gibt es informationen an anderer [Stelle](/mastodon/settings/konto-sicherheit.html#ziehe-zu-einem-anderen-konto-um).

## Ausgewählte Hashtags

Hier können Sie bis zu 10 [Hashtag](/mastodon/toots/hashtags.html)'s auswählen, die auf Ihrem Profil angezeigt werden sollen.
