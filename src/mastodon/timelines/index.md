# Zeitleisten

## Wer sieht wann meinen Beitrag

@foo – User einer anderen Instanz

@bar – Anderer User unserer Instanz

| Startseite                   | Lokale\*          | Föderierte\*                                         |
| ---------------------------- | ----------------- | ---------------------------------------------------- |
| Ich folge @foo und/oder @bar | Beiträge von @bar | @bar folgt @foo                                      |
| -                            | -                 | @bar boostet oder favorisiert den Beitrag von @foo   |
| -                            | -                 | @bar sucht den Beitrag von @foo per URL              |
| -                            | -                 | @bar folgt jemanden der auf @foo’s Beitrag antwortet |
| -                            | -                 | @bar folgt jemanden der @foo’s Beitrag boostet       |

\* Toots die mit „[Nicht gelistet](/mastodon/toots/visibility.html#nicht-gelistet)“ oder „[Nur für Folgende](/mastodon/toots/visibility.html#nur-f%C3%BCr-folger_innen)“ erstellt werden, werden hier niemals angezeigt

## Startseite

von manchen auch private/persönliche Zeitleite genannt

In der Startzeitleiste werden Beiträge von Nutzern angezeigt, denen Sie folgen. Je nachdem, was Sie in den obigen Einstellungen ausgewählt haben, werden auch geboostete Beiträge von Personen, denen Sie folgen, und Antworten von Personen, denen Sie folgen, angezeigt.
Was nicht angezeigt wird, sind z.B. Antworten von Personen, denen Sie nicht folgen, auf Beiträge von Personen, denen Sie folgen.

## Mitteilungen

Siehe dazu [Mitteilungen](/mastodon/mitteilungen/index.html)

## Entdecken

Entdecken zeigt beliebte Toot's, [Hashtag](/mastodon/toots/hashtags.html)'s und Nachrichten sowie neue oder aktive Benutzer\_innen aus Ihrer Instanz und von überall an.
In den Einstellungen können Sie festlegen, dass Sie nicht in Profilverzeichnissen angezeigt werden sollen.

## Lokale Zeitleiste

Zeigt alle Toots von Personen in Ihrer Instanz an. Boosts und Antworten werden nicht angezeigt.
Wenn Sie sich auf einer Instanz mit einem bestimmten Thema oder für einen bestimmten Ort befinden, ist die lokale Zeitleiste in der Regel interessanter als auf einer Instanz für allgemeine Zwecke.

### Nur Medien

In der oberen rechten Ecke können Sie auswählen, ob nur Beiträge mit Medienanhängen in der lokalen Zeitleiste angezeigt werden sollen.

## Föderierte Zeitleiste

Hier stehen all die Trööts, die in all den Instanzen geschrieben werden, die mit deiner Instanz verbunden sind, weil Accounts von deiner Instanz, Accounts von anderen Instanzen folgen, oder mit ihnen interagieren.

### Nur Medien

In der oberen rechten Ecke können Sie auswählen, ob nur Beiträge mit Medienanhängen in der lokalen Zeitleiste angezeigt werden sollen.

### Nur entfernt

Mit dieser Option werden die Toots von Personen, die sich auf Ihrer Instanz befinden, aus der Zeitleiste des Verbunds ausgeblendet.

## Favoriten

Hier siehst du alle Toots, die Ihnen gefallen haben, beginnend mit deinem letzten Like.

## Lesezeichen

In der Lesezeichen-Spalte werden alle Beiträge angezeigt, die Sie mit einem Lesezeichen versehen haben.
Wenn Sie nicht mehr möchten, dass ein Eintrag hier angezeigt wird, klicken Sie auf das Dropdown-Menü des Eintrags und wählen Sie "Lesezeichen entfernen".

## Listen

Hier können Sie Listen erstellen und ansehen.
Listen sind Zeitleisten, die nur Toots von Personen enthalten, die Sie auf diese Liste gesetzt haben.
So können Sie z.B. eine Liste mit Personen erstellen, die sehr interessante Beiträge veröffentlichen, damit Sie keinen ihrer Beiträge verpassen.
In einer Liste werden nur neue Beiträge von Nutzer\_innen angezeigt, seit sie der Liste hinzugefügt wurden.

## Analogie

Ich habe letzens einen Toot gelesen wo ein Erklärungsversuch mit einem Dorf versucht wurde, das fand ich eigentlich ganz gut und übernehme das hier.

### Startseite

Ist so wie etwa dein Briefkasten (ja das stammt aus Zeiten vor der E-Mail). Dort kommt nur das rein wofür du dich angemeldet hast (wem du folgst). Vorteil ist hier gibt es wirklich keine Werbung ;)

### Lokale Zeitleiste

Kann man sich vorstellen wie der Dorfplatz, dort kommen die Gespräche zusammen, die sich rund um das Dorf (die Instanz, auf der ihr seid) so mit bekommen könnt.

### Föderierte Zeitleiste

Das wäre dann etwa wie eine Zeitung oder Fernsehen, ihr bekommt dort alles(soweit der Instanz bekannt) mit, was so außerhalb eures Dorfes (Instanz) los ist.
