# Interaktion mit Profilen

## Folgt dir

Wenn der/die Nutzer\_in Ihnen folgt, wird dies oben auf dem Profil angezeigt.

## Folgen/Entfolgen

Als nächstes sehen Sie die Schaltfläche "Folgen" oder "Nicht folgen". Wenn Sie auf "Folgen" klicken, kann es sein, dass statt der Schaltfläche "Entfolgen" eine Schaltfläche "Folgeanfrage abbrechen" erscheint.
Das passiert, wenn die andere Person Ihr Profil gesperrt hat.

## Mich benachrichtigen

Wenn du dem Profil folgst, siehst du ein Glockensymbol neben der Schaltfläche "Nicht folgen". Klicken Sie darauf, um jedes Mal benachrichtigt zu werden, wenn der/die Nutzer\_in etwas postet.

## Mehr

Die folgenden Funktionen finden Sie im Dropdown-Menü - das letzte Symbol in der Reihe.

### @KONTONAME erwähnen

Mit dieser Funktion wird ein neuer Beitrag erstellt, in dem der/die Autor\_in des Toots erwähnt wird. Dies ist nicht mit einer Antwort vergleichbar, da der Beitrag, den Sie starten, nicht unter dem ursprünglichen Beitrag angezeigt wird.

### Direktnachricht @KONTONAME

Dies ist wie die Erwähnungsfunktion, außer dass die Sichtbarkeit des gestarteten Beitrags auf "direkt" gesetzt wird.

### Geteilte Beiträge von @KONTONAME verbergen

Damit werden Boosts dieses Nutzers in Ihren öffentlichen Timelines ausgeblendet. Dies kann nützlich sein, wenn die Person zwar interessante Dinge anpreist, aber zu viele uninteressante Dinge teilt.

### Auf Profil hervorheben

Du kannst die Profile anderer Nutzer auf deinem Profil in einem separaten Tab vorstellen.
Sie werden dann als Empfehlungen unter Ihrem Lebenslauf angezeigt.

### Hinzufügen oder Entfernen von Listen

Hier können Sie das Profil, das Sie gerade betrachten, direkt zu einer Liste oder zu Listen hinzufügen oder aus ihnen entfernen.

### @KONTONAME stummschalten

Wenn Sie einen Nutzer stumm schalten, werden Toots von ihm und Toots, die ihn erwähnen, ausgeblendet, aber er kann Ihre Toots weiterhin sehen und Ihnen folgen.
Sie können auswählen, ob Sie weiterhin Benachrichtigungen erhalten, wenn der Nutzer mit Ihnen interagiert, und wie lange die Stummschaltung andauern soll.

### @KONTONAME blockieren

Durch das Blockieren eines Nutzers werden seine Toots und die Toots, die ihn erwähnen, ausgeblendet und der Nutzer kann Sie und Ihre Toots nicht mehr sehen und mit Ihnen interagieren.

### @KONTONAME melden

Das Melden eines Benutzers sendet einen Bericht an den Administrator Ihrer Instanz und - wenn Sie es wünschen - auch an den Administrator der Instanz, in der sich der Benutzer befindet.
Du kannst die Beiträge des Benutzers auswählen, die in den Bericht aufgenommen werden sollen, und ein wenig darüber schreiben, warum du ihn meldest.

### Alles von INSTANZNAME verstecken

Diese Funktion sperrt nicht nur den Benutzer, sondern die gesamte Instanz, in der er sich befindet. Dies wird z.B. verwendet, um Spam, Trolling und Belästigung durch eine ganze Instanz zu bekämpfen.
Wenn du eine Instanz sperrst, ist es normalerweise eine gute Idee, auch einen Bericht oder eine direkte Nachricht an deinen Instanzadministrator zu schicken, damit er entscheiden kann, ob er die Domain für alle in deiner Instanz sperren will.
