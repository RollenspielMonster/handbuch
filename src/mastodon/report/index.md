# Probleme Melden

Wenn ihr problematische Inhalte oder problematische Accounts entdeckt meldet diese bitte. Dies geht ganz einfach über das 3-Punkte-Menü, egal ob bei einem Beitrag oder einem Profil.

**Wenn ihr einen Beitrag meldet, versucht immer die vorgefertigten Regeln zu benutzen.**

![Problem Melden](img/report.png)

## Von anderen Instanzen

Wenn der Account oder der Beitrag von einer anderen Instanz stammt habt ihr auch die Möglichkeit die andere Instanz zu benachrichten. Dazu solltet ihr euch aber am besten über deren Regeln schlau machen, das geht meist immer über `instanzurl/about/more`. **Gibt diese bitte dann immer auch in den Kommentaren an** z.B. `Extern: Regel` das würde unsere Arbeit dann sehr erleichtern.

Bitte lest euch dazu auch [Ebenen der Verantwortung](https://handbuch.rollenspiel.monster/mastodon/report/#ebenen-der-verantwortung) durch, ab wan es ein Problem für die Instanz ist oder nur euch selbst stört.

### Kommentare

Bitte stattet eure Meldungen immer mit Kommentaren aus, so das die Moderation weiß was euch genau stört und worum es geht. Ansonsten müssten wir euch immer anschreiben und nachfragen was die Moderation sehr viel Zeitintensiver gestalten würde.

## Apps

Jede APP verhält sich ein wenig anders bei der Melden-Funktion:

### Tusky

Auch hier wählt man bei dem Beitrag den man Melden möchte die 3 Punkte aus und geht dann auf Melden.

| Schritt 1                                                                                               | Schritt 2                                   |
| ------------------------------------------------------------------------------------------------------- | ------------------------------------------- |
| Nach der Auswahl "Melden" können weitere Beiträge dafür ausgewählt werden <img src="img/tusky1.png" alt="Tusky Melden" width="326.75"/> | Danach kann der Kommentar eingegeben werden. Siehe dazu oben. <img src="img/tusky2.png" alt="Tusky Melden" width="326.75"/> |

### Fedilab

Hier ist es ein wenig minimalistischer gehalten. Gleich nach dem Melden Knopf kommt folgendes Fenster wo die unscheinbare blaue Leiste das Textfeld für die Kommentare ist. Sobald ihr JA drückt wird der Beitrag gemeldet.

<img src="img/fedilab1.png" alt="Fedilab Melden" width="326.75"/>

### Metatext

Nachdem Melden ausgewählt worden ist steht auch hier ein Textfeld für Kommentare zur verfügung. Außerdem können weitere Beiträge markiert werden die ebenfalls zu dieser Meldung zusammengefasst werden sollen.

<img src="img/metatext1.jpg" alt="Fedilab Melden" width="326.75"/>

### Toot!

Fehlendes Testgerät

## Ebenen der Verantwortung

Das Fediverse ist ein großes Konstrukt und bringt auch ebenso viele Gefahren mit sich wie Möglichkeiten, auf eine davon möchte ich hier eingehen. Auch soll sie aufzeigen, wer für was verantwortlich ist.

### Unterschiedliche Instanzregeln

Wie viele sicher schon mitbekommen haben, handelt es sich im Fediverse um verschiedene Instanzen, die auch oft verschiedene Verhaltensregeln haben. Damit ist also sehr wahrscheinlich, dass bestimmte Inhalt in der Instanz A noch zu den Regeln passen, aber so nicht mehr bei der Instanz B geduldet werden. Das heißt aber nicht, dass wir Instanzen sperren werden, nur weil sie sich nicht an unsere Verhaltensregeln halten. Ausnahmen sind z. B. illegale oder in Deutschland strafbare Inhalte.

### Melden?

Das bedeutet für euch aber auch, dass ihr solches Verhalten nicht immer gleich melden müsst. Wenn euch ein Beitrag von xy nicht gefällt, könnt ihr die Person selbst [stumm schalten](/mastodon/interaktion/posts.html#kontoname-stummschalten) oder [blockieren](/mastodon/interaktion/posts.html#kontoname-blockieren), oder wenn euch der Inhalt der Instanz nicht gefällt, könnt ihr auch die ganze Instanz [verstecken/ausblenden](/mastodon/interaktion/posts.html#alles-von-instanzname-verstecken).

### Melden !

Wenn die Person von unserer Instanz ist und gegen eine Regel verstößt, könnt ihr sie [melden](/mastodon/interaktion/posts.html#kontoname-melden). Wenn sie von einer anderen Instanz ist und gegen deren Regeln oder gute Sitten verstößt, könnt ihr sie ebenfalls [melden](/mastodon/interaktion/posts.html#kontoname-melden), aber achtet darauf, dass ihr "An xy weiterleiten" aktiviert habt. Damit kann die entfernte Instanz sich um den problematischen Nutzer kümmern.

### Instanzblocks

Instanzblockaden sollten das letzte Mittel sein, welches von Administratoren genutzt wird, da es das potenzial besitzt das Fediverse zu zerstören. Denn wenn eine Instanz blockiert wird, ist es für keinen User der beiden Instanzen mehr möglich miteinander zu kommunizieren.

Es gibt gelegentlich Fälle, in denen Gruppen absichtlich Instanzen versuchen in Verruf zu bringen, um sie vom Rest des Fediverse (der Welt) abzuschneiden. z. B. könnte es genutzt werden, um in einem Land den Informationsfluss zu stoppen.


