# Beiträge (Toots, Tröts)

Eine wesentliche Information vorweg: bei Mastodon haben die Beiträge oder Nachrichten einen eigenen Namen, nämlich "Toot". Für die deutsche Sprache ist der Begriff "Tröt" im Umlauf.

Die Funktion zur Eingabe eines Toot ist in der Weboberfläche praktisch unübersehbar, links oben.

## Erster Toot

Viele stellen sich mit dem ersten Toot (ich benutze es liber als Tröt) erst einmal vor, wenn ihr das auch gerne machen wollt benutzt dabei den [Hashtag](/mastodon/toots/hashtags.html) `#NeuHier`

## Links zu GAMAM und anderen Datenkraken

Es wäre schön wenn ihr Links die ihr zu [GAMAM](https://de.wikipedia.org/wiki/Big_Tech) oder anderen Diensten die die privatsphäre ihrer User nicht respektieren über alternative Frontends verlinken würdet.

- Twitter -> [Nitter](https://nitter.net)
- Youtube -> [Piped](https://piped.kavin.rocks) oder [Invidious](https://invidious.snopyta.org)
- Instagram -> [Bibliogram](https://bibliogram.art)
- Reddit [libreddit](https://libredd.it)
