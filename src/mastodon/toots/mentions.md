# Adressieren von Nutzer\*innen

Wenn Du in einem Toot eine\*n andere\*n Nutzer\*in erwähnen oder ansprechen möchtest, kannst Du einfach ihren/seinen Nutzer\*innennamen in der üblichen Schreibweise

    @user@domain

verwenden. Wichtig ist, dass ein @-Zeichen am Anfang des Namens steht.

**Sprach-Hinweis:** Im Englischen heißen solche Erwähnungen deswegen auch _@-mentions_ bzw. _at-mentions_.

Sollte also der Benutzername beispielsweise `tealk` lauten, und die Domain `rollenspiel.social`, könnt Ihr die Nutzerin so ansprechen:

    @tealk@rollenspiel.social

Was passiert, wenn Du jemanden auf diese Art in einem Beitrag erwähnst?

- Die/der angesprochene Nutzer\*in erhält eine Benachrichtigung und bekommt Deinen Beitrag in der Liste der _Mitteilungen_, und zwar sowohl unter _Alle_ als auch unter _Erwähnungen_, sehen.

- Der erwähnte Name wird im Beitrag als Hyperlink dargestellt. Dabei ist es üblich, den Teil ab dem zweiten @-Zeichen, also die Domain der Instanz, zu verbergen. Die Mastodon-Weboberfläche zeigt Dir beim Klick auf einen solchen verlinkten Namen das Profil der Nutzerin bzw. des Nutzers an.

## Gruppen

In Mastodon gibt es eigentlich keine Gruppen, da hilft ein kleines Script weiter das unter https://a.gup.pe [Quellcode](https://github.com/wmurphyrd/guppe) läuft.
Guppe-Gruppen sehen aus wie normale Benutzer, mit denen Sie über Ihr bestehendes Konto bei einem beliebigen ActivityPub-Dienst interagieren können, aber sie teilen automatisch alles, was Sie ihnen schicken, mit allen ihren Anhängern.

Wenn ihr z.B. Fragen habt die ihr hier im Handbuch nicht beantwortet findet könnt ihr z.B. einfach `@askfedi_de@a.gup.pe` anschreiben.

### Wie funktioniert Guppe

- Folgen Sie einer Gruppe auf `@GRUPPENNAME@a.gup.pe`, um dieser Gruppe beizutreten
- Erwähnen Sie eine Gruppe auf `@GRUPPENNAME@a.gup.pe`, um einen Beitrag mit allen Mitgliedern der Gruppe zu teilen
- Neue Gruppen werden auf Anfrage erstellt, suchen Sie einfach nach `@GRUPPENNAME@a.gup.pe` oder erwähnen Sie es und es wird angezeigt.
- Besuchen Sie das Profil einer @a.gup.pe-Gruppe, um den Gruppenverlauf zu sehen ()
