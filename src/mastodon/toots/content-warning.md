# Inhaltswarnung (CW)

Ähnlich wie bei [Bildern und Videos](/mastodon/toots/media.html#medien-als-heikel-markieren) bietet Mastodon bei ganzen Beiträgen die Möglichkeit, zunächst eine Warnung anzuzeigen, die der/die Benutzer\*in anklicken muss, um den gesamten Inhalt des Beitrags anzuzeigen.

Die Funktion erreichst Du in der Web-Oberflächte über die mit `CW` für _Content Warning_ beschriftete Schaltfläche unterhalb des Eingabefeldes für Beiträge.

![Eingabefeld für den Nachrichtentext](img/toot-form-screenshot.png)

Nach dem Klick auf die Schaltfläche erscheint ein neues Eingabefeld oberhalb des Feldes für den Nachrichtentext, das mit _Inhaltswarnung_ beschriftet ist. In dieses Feld _kannst_ Du eine Überschrift bzw. die Warnung eingeben, die Nutzern zunächst angezeigt wird, wenn sie Deinen Beitrag sehen.

![Feld Inhaltswarnung](img/content-warning-form.png)

In den Zeitleisten anderer Nutzer\*innen wird ein solcher Toot so angezeigt wie auf dem nächsten Bild zu sehen.

![Anzeige Inhaltswarnung](img/content-warning-display.png)

Zunächst ist der Inhalt des Beitrags, einschließlich aller Bilder und Medien, verborgen. Erst durch Klick auf die Schaltfläche `MEHR ANZEIGEN` wird der gesamte Inhalt des Beitrags angezeigt.

## Was gehört hinter die CW?

Es gibt wohl auf Mastodon ein paar Dinge, die üblicherweise hinter CW versteckt sind, unter anderem fallen darunter:

- Dinge die Andere Ängstigen oder Verstören können (Gewalt, Waffen, Horro usw.)
- Augenkontakt (von Menschen - Fotos, Tieren und Kunst)
- Essen (Diskussionen über Essen und Essensbilder, real oder als Kunst)
- Alkohol
- Großbuchstaben (Schreie in Großbuchstaben)
- Spinnen, Reptilien & Insekten
- Gängige Phobien wie [Trypophobie](https://de.wikipedia.org/wiki/Trypophobie)
- Diskussion über Politik & Religion
- Werbung

**Kommunikation**: Wenn ihr etwas postet, von dem jemand meint, dass es mit CW hätte gepostet werden sollen, werdet ihr wahrscheinlich gefragt, ob ihr es noch einmal mit CW posten könntet. Solange ihr ihr dafür offen seid, kann es ein schönes Miteinander werden. Genauso ist es euch erlaubt andere darauf hinzuweisen wenn Sie Eurer Meinung nach heikle Inhalte nicht markiert haben oder nicht von der Inhaltswarnung Gebrauch gemacht haben, wo es angemessen wäre.
