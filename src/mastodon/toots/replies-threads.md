# Antworten und Threads

## Die Anwort-Funktion

Die Mastodon Web-Oberfläche, aber auch die anderen [Apps und Clients](/mastodon/apps-clients.html), erlauben das direkte Antworten auf einen Toot mit einem Klick.

In der Web-Oberfläche ist das Symbol, das hierfür angeklickt werden muss, ein hakenförmiger Pfeil nach links und befindet sich unter dem Inhalt des Toots links. Neben diesem Pfeil wird auch die Anzahl der Antworten angezeigt, die der Toot bereits erhalten hat.

![Antwort-Button](img/toot-reply-button.png)

Durch den Klick ändert sich das Eingabefeld für das Verfassen eines Toots. Darüber wird der Inhalt des Toots, auf den man antwortet, noch einmal dargestellt. Im Eingabefeld selbst wird der Name eines oder mehrerer Benutzerprofile eingefügt.

![Antwort-Eingabeformular](img/toot-reply-form.png)

Dahinter kannst Du nun den Antworttext eingeben, wie Du es bei einem normalen Beitrag tun würdest, und diesen veröffentlichen.

## Antwort vs. Erwähnung

Auch wenn eine Antwort so aussieht, als handele es sich dabei um einen Beitrag mit Erwähnung eines Benutzerprofils (oder mehrerer Benutzerprofile), so gibt es einen entscheidenden Unterschied:

ℹ️ **Wenn Du auf einen Toot antwortest, entsteht eine explizite Beziehung zwischen dem ursprünglichen Beitrag und Deiner Antwort.**

Wie oben bereits erwähnt, bekommen alle Nutzer\*innen im Zusammenhang mit einem Toot angezeigt, wie viele Antworten dazu es bereits gibt. Alle diese Antworten bekommst Du angezeigt, wenn Du die Detailseite des Toots aufrufst.

![Antwort in Thread-Kontext](img/toot-reply-in-thread.png)

Die Abbildung oben zeigt die Detailansicht eines Toots, der selbst die Antwort auf einen anderen Toot ist, und noch eine weitere Anwort erhalten hat. Durch die Darstellung dieses Antwortverlaufs, oder engl. "Thread", kann eine Unterhaltung einfach nachvollzogen werden.

## Threads

Der Begriff _Thread_ (deutsch: Strang) steht in digitalen Kommunikationswerkzeugen für die Möglichkeit, einzelne Beiträge im Kontext der Beiträge, auf die diese sich beziehen, anzuzeigen. Es gibt diese Möglichkeit auch in vielen E-Mail-Programmen.

Das kannst Du genau so auch auf Mastodon machen, indem Du nach dem Veröffentlichen eines Toots wie oben beschrieben die Anworten-Funktion zu Deinem eigenen Toot anklickst. Oft wird am ende einer Nachricht dann angegeben um die wievielte Beitrag es sich handelt z.B. 2/x heißt es ist der zweite Toot.
Am besten stellt ihr für jede Antwort in eurem Thread "[Nicht gelistet](/mastodon/toots/visibility.html#nicht-gelistet)" ein, somit wird verhindert das die ganze [Lokale Zeilteiste](/mastodon/timelines/#lokale-zeitleiste) mit jedem einzelnen Eintrag gefüllt ist.

Besser wäre es aber wenn ihr dafür Software benutzen würdet die dafür ausgelegt ist z.B. [Writefreely](https://blog.rollenspiel.monster/) oder [Lemmy](https://lemmy.rollenspiel.monster/). Damit könnt ihr lange Texte verfassen und diese über das Fediverse teilen.

## Beitrag Zitieren/Boosten mit Kommentar

Diese Funktion wurde absichtlich nicht in Mastodon eingebaut.

> Ich habe mich bewusst gegen eine Zitierfunktion entschieden, weil sie unweigerlich das Verhalten der Menschen vergiftet. Man ist versucht zu zitieren, wenn man eigentlich antworten sollte, und so spricht man zu seinem Publikum, anstatt mit seinem Gesprächspartner. Es wird zur Show. Selbst wenn man es für einen "guten Zweck" tut, z. B. wenn man sich über unangenehme Kommentare lustig macht, verschafft man unangenehmen Kommentaren auf diese Weise mehr Aufmerksamkeit. Kein Zitat. Dankeschön

[Quelle](https://mastodon.social/@Gargron/99662106175542726)
