# Fortgeschrittene Benutzeroberfläche

![Mastodoin UI](img/fortgeschritten.jpg)

Für die Erklärungen der Funktionen der linken Spalte schaut euch bitte die [Einfache Benutzeroberfläche](/mastodon/ui/simple.html) an.

## Aktivieren

Unter Einstellungen -> Aussehen könnt ihr die [Fortgeschrittene Benutzeroberfläche](/mastodon/settings/einstellungen.html#fortgeschrittene-benutzeroberfl%C3%A4che) aktivieren.

## Spalten

Die Fortgeschrittene Benutzeroberfläche wird in mehreren Spalten angezeigt, am Anfang habt ihr nur 2 Stück. Die [Startseite](/mastodon/timelines/#startseite) und die [Mitteilungen](/mastodon/mitteilungen/).
Das Tolle an dieser Ansicht ist, das ihr die Spalten so organisieren könnt wie ihr es wollt.

### Spalten hinzufügen

![Mastodoin UI](img/anheften.jpg)

Wenn ihr z.B. aus der Navigationsleiste `Erste Schritte` die `Lokale Zeitleiste` auswählt und dann oben rechts auf die Optionen geht, könnt ihr auf _Anheften_ klicken um eure Ansicht zu erweitern. Wie auf dem Screen zu sehen ist geht das auch mit [Hashtag]](/mastodon/toots/hashtags.html)'s.

#### Hashtag Spalte

![Mastodoin UI](img/hashtags.jpg)

Bei dieser Spalte gibt es noch eine Funktion die ihr erst seht wenn diese angeheftet ist. Öffnet das Optionsmenü erneut um mehreren Hashtags zu folgen oder z.B. nur Hashtags aus der eigenen Instanz zu folgen.

### Spalten entfernen

Dies funktioniert genauso wie Spalten hinzufügen, einfach bei bestehenden Spalten auf das Optiosmenü klicken und lösen auswählen.
