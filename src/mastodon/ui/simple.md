# Einfache Benutzeroberfläche

![Mastodoin UI](img/einfach.jpg)

Diese Webanwendung, oder das Web User Interface (UI), sind sowohl mit Desktop-Browsern am PC als auch mit gängigen Browsern auf dem Smartphone nutzbar.

Weil Mastodon auf dem offenen Standard [ActivityPub](activitypub.md) aufbaut, gibt es eine Reihe von Möglichkeiten, mit spezialisierten Anwendungen Beiträge zu erstellen, zu veröffentlichen, zu favorisieren und zu boosten, sowie Nutzern\_innen zu folgen etc.

## Suche

Sie können nach Personen und [Hashtag](/mastodon/toots/hashtags.html)'s und - in einigen Fällen - nach Beiträgen suchen.
Sie können Konten finden, indem Sie entweder nach ihren Profilnamen oder ihren Anzeigenamen suchen. Um ein Konto zu finden, das Sie noch nie zuvor gesehen haben, müssen Sie das gesamte Handle (@profilename@instance.domain) oder die URL des Profils eingeben. Wenn Sie nach einem Hashtag suchen, werden Ihnen alle Hashtags angezeigt, die mit dem von Ihnen verwendeten Suchbegriff beginnen. Wenn Sie also z.B. nach "rollenspiel" suchen, werden die Ergebnisse "RollenspielMonster" enthalten, wenn Sie aber nach "monster" suchen, werden diese nicht angezeigt.

Mehr dazu findet man auch im Kapitel [Wie folgt man?](/mastodon/network/following.html#wie-folgt-man)

## Ankündigungen

Wenn Sie auf der Oberfläche im Uhrzeigersinn weitergehen, ist die nächste Funktion die Anzeige von Ankündigungen. Sie ist hinter dem Megaphon-Symbol versteckt.
Wenn der Instanzadministrator eine Ankündigung geschrieben hat, wird sie hier angezeigt. Wenn es mehr als eine Ankündigung gibt, können Sie auf die Pfeilsymbole klicken, um sie alle zu sehen.
Sobald Sie alle Ankündigungen gelesen haben, verschwindet der blaue Punkt auf dem Megaphonsymbol, bis eine neue Ankündigung erscheint.

## Einstellungen in der Zeitleiste

Rechts neben dem Megaphonsymbol befindet sich ein Einstellungssymbol. Klicken Sie darauf, um zu ändern, was in Ihrer Startzeitleiste angezeigt wird.
Sie können Boosts und Antworten ausblenden oder anzeigen. Standardmäßig werden sowohl Boosts als auch Antworten in Ihrer Startzeitleiste angezeigt.
