# Über Mastodon

## Geschichte

Mastodon ist der Name einer urzeitlichen [Rüsseltier-Gattung](https://de.wikipedia.org/wiki/Mastodonten) und der einer dezentralen [Social Networking und Microblogging Software](https://joinmastodon.org/).

Die Software, um die es hier natürlich geht, wird seit 2016 von [Eugen Rochko](https://mastodon.social/@Gargron) entwickelt.
Sie ist quelloffen, also Open Source.

## Allgemeines

Mastodon wird von einigen als **Microblogging-Dienst** bezeichnet. Die Möglichkeiten ähneln denen von [Twitter](https://twitter.com/), denn Nutzer\_innen können Beiträge mit Text, Bildern, Videos und Audio-Dateien mit der Öffentlichkeit teilen.

Mastodon ist außerdem, ebenfalls ähnlich Twitter, eine Plattform zum Aufbau eines digitalen **sozialen Netzwerks**.
Nutzer\_innen folgen anderen Nutzer\_innen und abonnieren damit ihre Beiträge.

## Dezentral was ist das?

Mastodon funktioniert **dezentral**. Während es bei Twitter oder auch Facebook nur eine einzige zentrale Instanz gibt (also die Domain `twitter.com` oder `facebook.com`), gibt es bei Mastodon beliebig viele Instanzen, die mit einander Beiträge austauschen können. Jede Mastodon-Instanz ist durch ihren **Domainnamen** von anderen Instanzen zu unterscheiden. Die Instanz rollenspiel.social heißt eben so, weil sie über die Domain `rollenspiel.social` zu erreichen ist.

Technisch kann man die Dezentralität von Mastodon mit der von **E-Mail** vergleichen. Hier gibt es eine Menge größere und kleinere Anbieter, die jeweils eigene Server unter dem eigenen Domainnamen (`gmail.com`, `outlook.com`, `web.de`, `mailbox.org` etc.) betreiben.
Prinzipiell kann jeder Mensch mit den entsprechenden Kenntnissen selbst eine Mastodon-Instanz aufsetzen.

Mastodon-Instanzen tauschen nicht nur mit anderen Mastodon-Instanzen Nachrichten aus.
Sie sind auch **kompatibel** mit anderen Plattformen, die das **ActivityPub-Protokoll** unterstützen.
Solche Plattformen existierten auch schon vor Mastodon, und heute gibt es eine ganze Palette davon. Weil sie ein verteiltes (föderiertes), dezentrales Ökosystem bilden, wird ihr Verbund auch das [Fediverse](https://de.wikipedia.org/wiki/Fediverse) genannt.

Die meisten dieser Plattformen sind wie Mastodon Open-Source-Software, was davon zeugt, dass sie von ihren Entwicklern geschaffen wurden, um die Welt ein Stück besser zu machen und den großen, zentralen Diensten eine Alternative entgegenzusetzen:

- Dienste, bei denen wir selbst über unsere Daten entscheiden.
- Bei denen Nutzer\_innen nicht automatisch jedes Detail ihrer Persönlichkeit an irgend ein Unternehmen verschenken.
- Bei denen wir als Betreiber einer Instanz selbst die Regeln der Community festlegen können und entscheiden können, wie wir mit Verstößen gegen diese Regeln umgehen wollen.
- Bei denen wir möglichst mit anderen kooperieren, anstatt Geld für den Zugang zu verlangen

## Instanzen und ihre Themen

Jede Mastodon-Instanz ist Teil eines großen Netzwerks, kann aber auch als eigene, lokale Community-Plattform verstanden werden. Wer eine Mastodon-Instanz betreibt, hat selbst die Möglichkeit, Themenschwerpunkte und Regeln für diese festzulegen und kann selbst entscheiden, wer auf dieser Instanz ein Benutzer\_innenkonto einrichten kann.

Das Instanzen Themen haben heißt aber nicht das du über nichts anderes Schreiben darfst. Die Idee dahinter ist eine lokale Timeline die sich einem Thema widmet und das Neuankömmlinge schneller andere Leute mit gleichen Interessen finden. Ihr könnt also problemlos über andere Themen schreiben.

## Unterstützung

Wer möchte, kann die Weiterentwicklung von Mastodon über [Patreon](https://www.patreon.com/mastodon) finanziell unterstützen.
