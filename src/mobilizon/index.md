# Mobilizon

Mobilizon funktioniert wie viele andere Event Webseiten.
Du kannst Events erstellen und unter anderem Ort, Datum und Bilder hinzufügen.
Du kannst Mobilizon Nutzer\_innen einladen, aber auch andere Leute via Email.

## Gruppen

Registrierte User können sich in Gruppen zusammen tun und unter dem Gruppennamen Events erstellen.
Außerdem besitzen Gruppen eine Art Forum, Ressourcen (Tabellen und Textdateien) und eine öffentliche Seite für News.

## Folgen

Mobilizon spricht ebenfalls mit dem Fediverse und daher ist es auch möglich die Events über andere Fediverse Instanzen zu verfolgen.

### Ganzer Instanz

Es ist möglich einer ganzen Mobilizon-Instanz zu folgen, und zwar wenn man nach `@relay` sucht. In unserem Fall wäre das dann `@relay@rollenspiel.events`

### Einzelnem User/Gruppe

Einzelnen Gruppen oder User folgt man wie überall im Fediverse über den Namen. Z.B. unsere Schnupperrunden Gruppe findet man dann unter `@schnupperrunde@rollenspiel.events`

## Einschränkungen

Es ist aktuel nicht möglich aus dem [Fediverse](/fediverse/) an Events teilzunehmen, dies geht nur über einen Mobilizon Account oder als Gast (per Mail) falls der Ersteller des Events dies freigeschaltet hat.
