# Ressourcen der Gruppe

Ein Ort, um Links zu Dokumenten oder Ressourcen jeder Art zu speichern.

Im Abschnitt **Ressourcen** können Sie hinzufügen:

* Ordner
* Weblinks
* Tabelle
* Pad
* Videokonferenz

![Bild Ressourcen hinzufügen](img/add.png)

## Zugang zu den Ressourcen

Um auf Ihre Gruppenressourcen zugreifen zu können, müssen Sie **mindestens** ein Mitglied dieser Gruppe sein. Dann müssen Sie:

1. Klicken Sie auf **Meine Gruppen** im oberen Menü
* Klicken Sie auf die Gruppe, von der Sie auf die Ressourcen zugreifen möchten
* Klicken Sie auf **Alles anzeigen** neben Ressourcen

![Titel Ressourcenabschnitt](img/all.png)

## Hinzufügen einer Ressource

Um eine Ressource hinzuzufügen, müssen Sie auf der Ressourcenseite auf die Schaltfläche **+** klicken und die Art der Ressource auswählen, die Sie hinzufügen möchten:

![Bild Ressourcen hinzufügen](img/add.png)

### Einen neuen Ordner hinzufügen

Um einen neuen Ordner hinzuzufügen, müssen Sie, auf der Ressourcen-Seite:

1. Klicken Sie auf die Schaltfläche **+** (siehe oben)
* Wählen Sie **Neuer Ordner**
* geben Sie einen Namen für diesen Ordner ein
* Klicken Sie auf die Schaltfläche **Ordner erstellen**.

### Einen neuen Link hinzufügen

Um einen neuen Link hinzuzufügen, müssen Sie auf der Ressourcenseite:

1. Klicken Sie auf die Schaltfläche **+** (siehe oben)
* Wählen Sie **Neuer Link**
* Fügen Sie den Weblink hinzu
* [optional] fügen Sie einen Titel hinzu (dieser wird automatisch hinzugefügt, wenn er leer gelassen wird)
* [optional] fügen Sie eine Beschreibung hinzu (diese wird automatisch hinzugefügt, wenn sie leer gelassen wird)
* Klicken Sie auf die Schaltfläche **Ressource erstellen**.

### Hinzufügen einer Tabelle

Um eine neue Tabelle hinzuzufügen, müssen Sie auf der Ressourcen-Seite:

1. Klicken Sie auf den **+** Button (siehe oben)
* Wählen Sie **Tabelle erstellen**
* wählen Sie einen Namen für diese Berechnung
* Klicken Sie auf die Schaltfläche **Tabelle erstellen**.

!!! Hinweis
    Diese Kalkulation wird auf einer externen Website erstellt (konfigurierbar durch den Instanzmanager)

### Pad hinzufügen

Um ein neues Pad hinzuzufügen, müssen Sie auf der Ressourcen-Seite:

1. Klicken Sie auf die Schaltfläche **+** (siehe oben)
* Wählen Sie **Pad erstellen**
* wählen Sie einen Namen für dieses Pad
* Klicken Sie auf die Schaltfläche **Pad erstellen**.

!!! Hinweis
    Dieses Pad wird auf einer externen Website erstellt (konfigurierbar durch den Instanzmanager)

### Videokonferenz hinzufügen

Um eine neue Videokonferenz hinzuzufügen, müssen Sie auf der Ressourcen-Seite:

1. Klicken Sie auf die Schaltfläche **+** (siehe oben)
* Wählen Sie **Videokonferenz erstellen**
* Wählen Sie einen Namen für diese Videokonferenz
* Klicken Sie auf die Schaltfläche **Videokonferenz einrichten**.

!!! Hinweis
    Diese Videokonferenz wird auf einer externen Website erstellt (konfigurierbar durch den Instanzmanager)

## Umbenennen einer Ressource

Um einen Ordner oder eine Datei umzubenennen, müssen Sie auf der Ressourcen-Seite:

1. auf **⋅⋅⋅** vor der Ressource klicken.
* anklicken **Umbenennen**
* Änderungen vornehmen
* Klicken Sie auf die Schaltfläche **Name der Ressource**.

## Verschieben einer Ressource

Sie können eine Datei in einen Ordner verschieben, oder einen Ordner in einen anderen, usw... Um dies zu tun, müssen Sie, auf der Ressourcen-Seite:

1. auf **⋅⋅⋅** vor der Ressource klicken
* klicken **Verschieben**
* Wählen Sie aus, wohin Sie die Ressource verschieben möchten.
* Sobald Sie sich an der richtigen Stelle befinden, klicken Sie auf die Schaltfläche ** Verschiebe Ressource in das [ORDNER] Verzeichnis** verschieben

## Löschen einer Ressource

Um eine Ressource zu löschen, müssen Sie, auf der Ressourcen-Seite:

1. auf **⋅⋅⋅** vor der Ressource klicken.
* anklicken **Löschen**