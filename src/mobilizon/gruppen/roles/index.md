# Rollen verwalten

## Rollen

| ACTIONS                                                             | ADMINISTRATOR | MODERATOR | MEMBER |
| ------------------------------------------------------------------- | ------------- | --------- | ------ |
| Eine Diskussion beginnen / bearbeiten / löschen                     | ✔             | ✔        | ✔ ¹    |
| Hinzufügen / Bearbeiten / Löschen einer Ressource                   | ✔             | ✔        | ✔      |
| Ein Ereignis erstellen / löschen                                    | ✔             | ✔        | ❌     |
| Veröffentlichen / Bearbeiten / Löschen einer öffentlichen Nachricht | ✔             | ✔        | ❌     |
| Einladungen verwalten²                                              | ✔             | ✔        | ❌     |
| Mitglieder hinzufügen                                               | ✔             | ❌       | ❌     |
| Rollen verwalten                                                    | ✔             | ❌       | ❌     |

¹ der die Diskussion erstellt hat
² abhängig von den Einstellungen

!!! note
    Wenn eine Person eingeladen wird und diese Einladung annimmt, um Ihrer Gruppe beizutreten, ist sie standardmäßig *nur* ein **Mitglied**.

## Befördern

Sie können ein Konto zum **Moderator** oder **Administrator** befördern, indem Sie:

1. Klicken Sie auf die Schaltfläche **Meine Gruppen** in der oberen Menüleiste
* die Gruppe anklicken, die Sie verwalten möchten
* Klicken Sie auf den Link **Hinzufügen / Entfernen...** in Ihrem Gruppenbanner
* Klicken Sie auf die Schaltfläche **Fördern** vor dem Benutzer, den Sie fördern möchten.

!!! info
    Du kannst immer nur **eine Stufe auf einmal** befördern: **Mitglied**, dann **Moderator**, dann **Administrator**.

## Degradieren

Sie können ein Konto zum **Administrator** degradieren, indem Sie:

1. Klicken Sie auf die Schaltfläche **Meine Gruppen** in der oberen Menüleiste
* Klicken Sie auf die Gruppe, die Sie verwalten möchten
* Klicken Sie auf den Link **Hinzufügen / Entfernen...** in Ihrem Gruppenbanner
* Klicken Sie auf die Schaltfläche **Entfernen** vor dem Benutzer, den Sie entlassen möchten.

!!! Info
    Sie können immer nur **eine Stufe auf einmal** degradieren: **Administrator**, dann **Moderator**, dann **Mitglied**.

## Entfernen

Um ein Mitglied zu entfernen, müssen Sie:

1. Klicken Sie auf die Schaltfläche **Meine Gruppen** in der oberen Menüleiste
* Klicken Sie auf die Gruppe, die Sie verwalten möchten
* klicken Sie auf den Link **Hinzufügen / Entfernen...** in Ihrem Gruppenbanner
* klicken Sie auf die Schaltfläche **Entfernen** vor dem Benutzer

!!! Hinweis
    Wenn der Benutzer ein Moderator oder ein Administrator ist, müssen Sie ihn zuerst degradieren.

