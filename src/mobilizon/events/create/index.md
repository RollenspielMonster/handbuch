# Ein Ereignis erstellen

Erstellen wir ein neues Ereignis!

Klicken Sie dazu auf die Schaltfläche **Erstellen** in der oberen Leiste. Dann müssen Sie einige Informationen über Ihre Veranstaltung eingeben, damit Sie die Leute zusammenbringen können!

!!! note
    Sie können jederzeit einen Entwurf Ihrer Veranstaltung speichern, indem Sie auf die Schaltfläche **Entwurf speichern** in der festen unteren Leiste klicken.
    ![bild: spiechern schaltfläche](img/save.png)

## Allgemeine Informationen

![bild: ereignis erstellen-leer](img/create.png)

In diesem Abschnitt können Sie:

* ein **Headline-Bild** hinzufügen, um ein Banner auf Ihrer Veranstaltungsseite zu haben, indem Sie auf **Klick zum Hochladen** klicken und ein passendes Bild auswählen
* einen **Titel** hinzufügen (erforderlich)
* eine **Kategorie** hinzufügen um das Event leichter einteilbar zu machen
* fügen Sie einige **Tags** (max. 10) hinzu, um Ihre Veranstaltung von anderen ähnlichen Veranstaltungen zu unterscheiden (Tags werden durch Drücken der "Enter"-Taste oder durch Trennen mit Kommas hinzugefügt)
* Legen Sie ein Start- und Enddatum für Ihre Veranstaltung fest. Sie können auch die Uhrzeit des Beginns/Endes der Veranstaltung anzeigen/ausblenden, indem Sie auf **Datumsparameter** klicken
* eine Adresse hinzufügen: Sie können entweder die Adresse eingeben oder Ihre Adresse durch Anklicken des Symbols lokalisieren. Sobald dies geschehen ist, wird der Standort auf einer OpenStreetMap-Karte angezeigt.
* Hinzufügen einer **Beschreibung**
* Hinzufügen einer Website oder einer beliebigen URL

## Organisatoren

In diesem Abschnitt können Sie ein Profil oder eine Gruppe als Organisator auswählen. Wenn Sie eine Gruppe auswählen, können Sie einen oder mehrere Kontakte aus der Gruppe auswählen.

## Metadaten

![image: Metadaten](img/meta.png)

Hier können Sie Metadaten eintragen z.B. einen link zu einem Ticketverkauf oder links zu anderen Fediversekonten. Es ist auch möglich seine eigenen Metadaten über **Neu hinzufügen...** einzutragen.


## Wer kann diese Veranstaltung sehen und teilnehmen

![image: Veranstaltung erstellen wer teilnimmt](img/users.png)

Hier können Sie festlegen, wer Ihre Veranstaltung sehen und an ihr teilnehmen kann!

* **Überall im Web sichtbar (öffentlich)**: jeder kann es sehen, wenn er Ihre Mobilizon-Instanz durchsucht
**Nur über einen Link zugänglich (privat)**: nur Personen, die den Link haben, können Ihre Veranstaltung sehen

**Anonyme Teilnahme**: wenn Sie Personen die Möglichkeit geben wollen, ohne ein Konto teilzunehmen.

!!! note
    Anonyme Teilnehmer werden gebeten, ihre Teilnahme per E-Mail zu bestätigen.

**Teilnahmegenehmigung**: wenn Sie jede Teilnahmeanfrage genehmigen möchten (Teilnahmebenachrichtigungen können [in Ihrem Profil] eingestellt werden (../../users/account-settings/#participation-notifications))

**Anzahl der Plätze**: wenn Sie diese Anzahl begrenzen möchten

## Öffentliche Kommentar-Moderation

![Bild: Kommentarmoderation](img/comments.png)

In diesem Abschnitt können Sie einstellen, ob Sie Kommentare zulassen oder nicht.

## Status

![Bild: Status Ihrer Veranstaltung](img/status.png)

Hier können Sie angeben, ob Ihre Veranstaltung vorläufig ist und später bestätigt wird, ob sie stattfinden wird oder ob sie abgesagt wurde.