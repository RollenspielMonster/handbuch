## Mumble

Für Mumble findet ihr hier eine sehr ausführliche Dokumentation: https://wiki.natenom.de/mumble/benutzerhandbuch/mumble.html

## Teamspeak

### (¯`·» Temp. Channel

Um einen eigenen Kanal zu erstellen müsst ihr diesem Kanal betreten und dann auf den selbigen mit der rechten Maustaste "Sub-Kanal erstellen" auswählen.

### (¯`·» Kampangen

Hier können wir euch einen (Semi-)Permanenten Kanal erstellen falls ihr längere Kampangen über unseren Server spielt.