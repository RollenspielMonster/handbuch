# Fediverse-Handbuch

[![Codeberg CI](https://ci.codeberg.org/api/badges/RollenspielMonster/handbuch/status.svg)](https://ci.codeberg.org/RollenspielMonster/handbuch)
![Framework](https://img.shields.io/badge/Framework-mdBook-pink)
![Lizenz](https://img.shields.io/badge/Lizenz-CC0%201.0-orange)

Dieses Repository enthält das Fediverse-Handbuch, das mit mdBook erstellt wurde. Das Handbuch bietet eine umfassende Sammlung von Erklärungen und Informationen zu den verschiedenen Diensten des Fediverse. Es ist als Webanwendung konzipiert und ermöglicht es Benutzern, das Handbuch interaktiv im Browser zu durchsuchen.

## Inhaltsverzeichnis

- [Fediverse-Handbuch](#fediverse-handbuch)
  - [Inhaltsverzeichnis](#inhaltsverzeichnis)
  - [Über das Fediverse-Handbuch](#über-das-fediverse-handbuch)
  - [Installation](#installation)
  - [Verwendung](#verwendung)
  - [Beitragende](#beitragende)
  - [Kontakt](#kontakt)
  - [Lizenz](#lizenz)

## Über das Fediverse-Handbuch

Das Fediverse-Handbuch ist eine umfangreiche Ressource, die Benutzern helfen soll, das Fediverse besser zu verstehen und die verschiedenen Dienste effektiv zu nutzen. Es enthält Erklärungen, Anleitungen und Tipps zu den wichtigsten Aspekten des Fediverse, einschließlich der Nutzung von sozialen Netzwerken, Identitätsprotokollen und Interaktionen zwischen den Diensten.

## Installation

Um das Fediverse-Handbuch lokal auszuführen, müssen Sie die folgenden Schritte ausführen:

1. Stellen Sie sicher, dass Sie eine aktuelle Version von Rust und Cargo auf Ihrem System installiert haben.

2. Klonen Sie das Repository mit Git:
```
git clone https://codeberg.org/RollenspielMonster/handbuch.git
```

3. Wechseln Sie in das Verzeichnis des Handbuchs:
```
cd handbuch
```

4. Führen Sie den Befehl `cargo install mdbook` aus, um mdBook auf Ihrem System zu installieren.

## Verwendung

Um das Fediverse-Handbuch zu öffnen, führen Sie den folgenden Befehl im Hauptverzeichnis des Handbuchs aus:
```
mdbook serve
```

Dies startet einen lokalen Webserver, der das Handbuch bereitstellt. Öffnen Sie dann Ihren Webbrowser und navigieren Sie zur angegebenen Adresse (normalerweise http://localhost:3000), um das Handbuch anzuzeigen und darin zu navigieren.

## Beitragende

Beiträge zum Fediverse-Handbuch sind willkommen! Wenn Sie Verbesserungen vornehmen möchten, führen Sie bitte die folgenden Schritte aus:

1. Forken Sie das Repository.
2. Erstellen Sie einen neuen Branch für Ihre Änderungen.
3. Führen Sie Ihre Änderungen durch und veröffentlichen Sie diese in Ihrem Fork.
4. Erstellen Sie einen Pull-Request, um Ihre Änderungen in das Hauptrepository einzubringen.

Wir schätzen Ihre Beiträge und werden sie überprüfen und zusammenführen, um das Handbuch kontinuierlich zu verbessern.

## Kontakt

Bei Fragen, Anregungen oder Diskussionen können Sie uns über Matrix kontaktieren. Besuchen Sie [https://matrix.to/#/#rm_handuch:tchncs.de](https://matrix.to/#/#rm_handuch:tchncs.de), um dem Raum beizutreten und mit uns in Kontakt zu treten.

## Lizenz

Das Fediverse-Handbuch steht unter der [CC0 1.0 Universal (CC0 1.0) Public Domain Dedication](LICENSE) Lizenz. Weitere Informationen finden Sie in der Lizenzdatei.
